//Carlos Manuel Gonz�lez Mira
import javax.swing.JOptionPane;

public class S09dEjercicio5 {

	public static void main(String[] args) {
		String numero = JOptionPane.showInputDialog("Introduce un n�mero:");
		int num = Integer.parseInt(numero);
		if (num%2==0)
			JOptionPane.showMessageDialog(null, "El n�mero "+num+" es par");
		else
			JOptionPane.showMessageDialog(null, "El n�mero "+num+" es impar");

	}

}
