//Carlos Nanuel Gonz�lez Mira
import javax.swing.JOptionPane;

public class S10bEjercicio3 {

	protected static boolean esPrimo (int num) {
		boolean primo = true;
		for (int i=2;i<num;i++) {
			if (num%i==0)
				primo = false;
		}
		return primo;
	}
	
	public static void main(String[] args) {
		String texto_numero = JOptionPane.showInputDialog("Introduce un n�mero");
		int numero = Integer.parseInt(texto_numero);
		boolean numEsPrimo = esPrimo(numero);
		if (numEsPrimo)
			JOptionPane.showMessageDialog(null, "Es primo");
		else
			JOptionPane.showMessageDialog(null, "No es primo");
	}

}
