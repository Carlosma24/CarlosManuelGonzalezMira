//Carlos Manuel Gonz�lez Mira
import javax.swing.JOptionPane;

public class S10bEjercicio2 {

	protected static int aleatorio (int min, int max) {
		int aleatorio = (int)(Math.random()*(max-min+1)+min);
		return aleatorio;
	}
	
	public static void main(String[] args) {
		String repeticiones = JOptionPane.showInputDialog("�Cuantos n�meros aleatorios quieres?");
		int numRep = Integer.parseInt(repeticiones);
		String textoMinimo = JOptionPane.showInputDialog("�Cu�l es el m�nimo?");
		String textoMaximo = JOptionPane.showInputDialog("�Cu�l es el m�ximo?");
		int minimo = Integer.parseInt(textoMinimo);
		int maximo = Integer.parseInt(textoMaximo);
		
		for (int i=0;i<numRep;i++ ) {
			JOptionPane.showMessageDialog(null, aleatorio(minimo,maximo));
		}
	}

}
