//Carlos Manuel Gonz�lez Mira
import javax.swing.JOptionPane;

public class S10bEjercicio1 {

	protected static double circulo (double radio) {
		double resul = Math.PI*(Math.pow(radio, 2));
		return resul;
	}
	
	protected static double triangulo (double base, double altura) {
		double resul = (base*altura)/2;
		return resul;
	}
	
	protected static double cuadrado (double lado) {
		double resul = Math.pow(lado, 2);
		return resul;
	}
	
	public static void main(String[] args) {
		String figura = JOptionPane.showInputDialog("Elige la figura de la que vamos a calcular el �rea:\na) C�rculo\nb) Tri�ngulo\nc) Cuadrado");
		switch (figura) {
			case "a": {
				String texto_radio = JOptionPane.showInputDialog("Introduce el radio del c�rculo (en cm):");
				double radio = Double.parseDouble(texto_radio);
				double resul = circulo(radio);
				JOptionPane.showMessageDialog(null, "El �rea del c�rculo es: "+resul+" cm2");
			}	break;
			case "b": {
				String texto_base = JOptionPane.showInputDialog("Introduce la base del tri�ngulo (en cm):");
				String texto_altura = JOptionPane.showInputDialog("Introduce la altura del tri�ngulo (en cm):");
				double base = Double.parseDouble(texto_base);
				double altura = Double.parseDouble(texto_altura);
				double resul = triangulo(base,altura);
				JOptionPane.showMessageDialog(null, "El �rea del tri�ngulo es: "+resul+" cm2");
			}	break;
			case "c": {
				String texto_lado = JOptionPane.showInputDialog("Introduce el lado del cuadrado (en cm):");
				double lado = Double.parseDouble(texto_lado);
				double resul = cuadrado(lado);
				JOptionPane.showMessageDialog(null, "El �rea del cuadrado es: "+resul+" cm2");
			}	break;
			default: 
				JOptionPane.showMessageDialog(null,"Esa no es una opci�n");
				break;
			
		}

	}

}
