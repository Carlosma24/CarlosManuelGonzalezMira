//Carlos Manuel Gonz�lez Mira
import javax.swing.JOptionPane;

public class S10bEjercicio4 {

	protected static long factorial(long n, long m) {
		m++;
		if (m==(n-1)) {
			n*=m;
			return n;
		}
		else
			return m*factorial(n,m);
	}
	
	public static void main(String[] args) {
		 String texto_num = JOptionPane.showInputDialog("Introduce un n�mero");
		 long num = Integer.parseInt(texto_num);
		 long resul = factorial(num,0);
		 JOptionPane.showMessageDialog(null,resul);

	}

}
