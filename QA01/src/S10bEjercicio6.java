//Carlos Manuel Gonz�lez Mira
 import javax.swing.JOptionPane;

public class S10bEjercicio6 {

	protected static int calculoCifras(String cadena) {
		return cadena.length();
	}
	
	public static void main(String[] args) {
		String numero = JOptionPane.showInputDialog("Introduce un n�mero");
		int numCifras = calculoCifras(numero);
		JOptionPane.showMessageDialog(null, numero+" tiene "+numCifras+" cifras");

	}

}
