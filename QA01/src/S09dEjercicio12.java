//Carlos Manuel Gonz�lez Mira
import javax.swing.JOptionPane;

public class S09dEjercicio12 {

	public static void main(String[] args) {
		String password = "Alumnos_16";
		int intentos = 3;
		boolean acierto = false;
		do {
			String pwUsuario = JOptionPane.showInputDialog("Introduce la contrase�a del wifi ("+intentos+" intentos)");
			if (pwUsuario.equals(password))
				acierto = true;
			intentos--;
		} while ((!acierto)&&(intentos>0));
		if (acierto)
			JOptionPane.showMessageDialog(null, "Enhorabuena");
		else
			JOptionPane.showMessageDialog(null, "Te has quedado sin intentos");

	}

}
