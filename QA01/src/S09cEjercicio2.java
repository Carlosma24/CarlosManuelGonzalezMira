//Carlos Manuel Gonz�lez Mira
public class S09cEjercicio2 {

	public static void main(String[] args) {
		int N = 42;
		double A = 3.14;
		char C = 'X';
		
		System.out.println("Variable N = "+N);
		System.out.println("Variable A = "+A);
		System.out.println("Variable C = "+C);
		System.out.println(N+" + "+A+" = "+(N+A));
		System.out.println(A+"- "+N+" = "+(A-N));
		System.out.println("Valor num�rico del car�cter "+C+" = "+(int)C);

	}

}
