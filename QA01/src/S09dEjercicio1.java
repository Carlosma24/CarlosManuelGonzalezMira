//Carlos Manuel Gonz�lez Mira
import javax.swing.JOptionPane;

public class S09dEjercicio1 {

	public static void main(String[] args) {
		int num1, num2;
		String textoNumero = JOptionPane.showInputDialog("Introduce un n�mero");
		num1 = Integer.parseInt(textoNumero);
		textoNumero = JOptionPane.showInputDialog("Introduce un n�mero");
		num2 = Integer.parseInt(textoNumero);
		
		if (num1>num2)
			JOptionPane.showMessageDialog(null, num1+" es mayor que "+num2);
		else if (num2>num1)
			JOptionPane.showMessageDialog(null, num2+" es mayor que "+num1);
		else
			JOptionPane.showMessageDialog(null, "Ambos n�meros son iguales");

	}

}
