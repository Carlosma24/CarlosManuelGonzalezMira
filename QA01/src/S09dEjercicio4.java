//Carlos Manuel Gonz�lez Mira
import javax.swing.JOptionPane;

public class S09dEjercicio4 {

	public static void main(String[] args) {
		String r = JOptionPane.showInputDialog("Introduce el radio: ");
		double radio = Double.parseDouble(r);
		double resul = Math.PI*(Math.pow(radio, 2));
		JOptionPane.showMessageDialog(null, "El �rea de un c�rculo de radio "+radio+" es: "+resul);

	}

}
