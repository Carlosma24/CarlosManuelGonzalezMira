//Carlos Manuel Gonz�lez Mira
import javax.swing.JOptionPane;

public class S09dEjercicio11 {

	public static void main(String[] args) {
		String diaSemana = JOptionPane.showInputDialog("Introduce un d�a de la semana");
		diaSemana = diaSemana.toLowerCase();
		switch (diaSemana) {
		case "lunes":	JOptionPane.showMessageDialog(null, "Es d�a laboral");
			break;
		case "martes":	JOptionPane.showMessageDialog(null, "Es d�a laboral");
			break;
		case "mi�rcoles":	JOptionPane.showMessageDialog(null, "Es d�a laboral");
			break;
		case "jueves":	JOptionPane.showMessageDialog(null, "Es d�a laboral");
			break;
		case "viernes":	JOptionPane.showMessageDialog(null, "Es d�a laboral");
			break;
		case "s�bado":	JOptionPane.showMessageDialog(null, "Es d�a no es laboral");
			break;
		case "domingo":	JOptionPane.showMessageDialog(null, "Es d�a no es laboral");
			break;
		default: JOptionPane.showMessageDialog(null, "�Ese d�a no existe, merluzo!");
		}

	}

}
