//Carlos Manuel Gonz�lez Mira
import javax.swing.JOptionPane;

public class S09dEjercicio13 {

	public static void main(String[] args) {
		String num1 = JOptionPane.showInputDialog("Introduce un n�mero");
		String num2 = JOptionPane.showInputDialog("Introduce otro n�mero");
		String signo = JOptionPane.showInputDialog("�Qu� operaci�n vamos a hacer?"
				+ "\nSuma= +"
				+ "\nResta = -"
				+ "\nMultiplicaci�n = *"
				+ "\nDivisi�n = /"
				+ "\nPotencia = P"
				+ "\nResto = %");
		
		int n1 = Integer.parseInt(num1);
		int n2 = Integer.parseInt(num2);
		
		switch (signo) {
			case "+": JOptionPane.showInputDialog("El resultado es: "+(n1+n2));
				break;
			case "-":	JOptionPane.showInputDialog("El resultado es: "+(n1-n2));
				break;
			case "*":	JOptionPane.showInputDialog("El resultado es: "+(n1*n2));
				break;
			case "/":	JOptionPane.showInputDialog("El resultado es: "+(n1/n2));
				break;
			case "P":	JOptionPane.showInputDialog("El resultado es: "+(Math.pow(n1, n2)));
				break;
			case "%":	JOptionPane.showInputDialog("El resultado es: "+(n1%n2));
				break;
			default:	JOptionPane.showMessageDialog(null, "�Qu� signo es ese? No lo pillo");
				break;
		}
	}

}
