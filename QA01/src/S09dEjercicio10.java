//Carlos Manuel Gonz�lez Mira
import javax.swing.JOptionPane;

public class S09dEjercicio10 {

	public static void main(String[] args) {
		String texto_numVentas = JOptionPane.showInputDialog("�Cu�ntas ventas vas a introducir?");
		int numVentas = Integer.parseInt(texto_numVentas);
		int total = 0;
		for (int i=0;i<numVentas;i++) {
			String provisional = JOptionPane.showInputDialog("Introduce la venta n�mero "+(i+1));
			total += Integer.parseInt(provisional);
		}
		JOptionPane.showMessageDialog(null, "La suma de todas las ventas es: "+total);

	}

}
