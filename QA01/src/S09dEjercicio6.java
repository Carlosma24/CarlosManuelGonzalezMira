//Carlos Manuel Gonz�lez Mira
import javax.swing.JOptionPane;

public class S09dEjercicio6 {

	public static void main(String[] args) {
		final double IVA = 0.21;
		String textoPrecio = JOptionPane.showInputDialog("Introduce el precio del producto");
		double precio = Double.parseDouble(textoPrecio);
		double precioTotal = precio+(precio*IVA);
		JOptionPane.showMessageDialog(null, "El precio con IVA es: "+precioTotal+"�");

	}

}
