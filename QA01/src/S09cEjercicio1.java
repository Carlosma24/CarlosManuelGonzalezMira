//Carlos Manuel González Mira
public class S09cEjercicio1 {

	public static void main(String[] args) {
		int numero1 = 24;
		int numero2 = 42;
		
		int suma = numero1+numero2;
		int resta = numero1-numero2;
		int multiplicacion = numero1*numero2;
		int division = numero1/numero2;
		int modulo = numero1%numero2;
		
		System.out.println("Suma: "+suma);
		System.out.println("Resta "+resta);
		System.out.println("Multiplicación: "+multiplicacion);
		System.out.println("División: "+division);
		System.out.println("Resto: "+modulo);
	}

}
